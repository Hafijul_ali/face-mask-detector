from sklearn.metrics import classification_report
import matplotlib.pyplot as plt
import numpy as np

def model_report(model, testX, testY, lb, BATCH_SIZE):
	predIdxs = model.predict(testX, batch_size=BATCH_SIZE)
	predIdxs = np.argmax(predIdxs, axis=1)
	print(classification_report(testY.argmax(axis=1), predIdxs,
		target_names=lb.classes_))


def plot_graph(train_history, plot_name, EPOCHS):
	plt.style.use("ggplot")
	plt.figure()
	plt.plot(np.arange(0, EPOCHS), train_history.history["loss"], label="train_loss")
	plt.plot(np.arange(0, EPOCHS), train_history.history["val_loss"], label="val_loss")
	plt.plot(np.arange(0, EPOCHS), train_history.history["accuracy"], label="train_acc")
	plt.plot(np.arange(0, EPOCHS), train_history.history["val_accuracy"], label="val_acc")
	plt.title("Training Loss and Accuracy")
	plt.xlabel("Epoch #")
	plt.ylabel("Loss/Accuracy")
	plt.legend(loc="lower left")
	plt.savefig(plot_name)