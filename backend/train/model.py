from tensorflow.keras.applications import MobileNetV2
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.models import Model
from tensorflow.keras.layers import (AveragePooling2D, 
	Dropout, 
	Flatten, 
	Dense,
	Input,
)

def get_model(INITIAL_LEARNING_RATE, EPOCHS):
	baseModel = MobileNetV2(weights="imagenet", include_top=False,input_tensor=Input(shape=(224, 224, 3)))
	headModel = baseModel.output
	headModel = AveragePooling2D(pool_size=(7, 7))(headModel)
	headModel = Flatten(name="flatten")(headModel)
	headModel = Dense(128, activation="relu")(headModel)
	headModel = Dropout(0.5)(headModel)
	headModel = Dense(2, activation="softmax")(headModel)

	model = Model(inputs=baseModel.input, outputs=headModel)

	for layer in baseModel.layers:
		layer.trainable = False

	opt = Adam(lr=INITIAL_LEARNING_RATE, decay=INITIAL_LEARNING_RATE / EPOCHS)
	model.compile(loss="binary_crossentropy", optimizer=opt, metrics=["accuracy"])
	return model


def save_model(model, name):
	model.save(name, save_format="h5")