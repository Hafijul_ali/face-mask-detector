from tensorflow.keras.preprocessing.image import (
	load_img,
	img_to_array,
	)
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from imutils import paths
import numpy as np
import os

def serialize_data(path):
	imagePaths = list(paths.list_images(path))
	data = []
	labels = []

	for imagePath in imagePaths:
		label = imagePath.split(os.path.sep)[-2]
		image = load_img(imagePath, target_size=(224, 224))
		image = img_to_array(image)
		image = preprocess_input(image)
		data.append(image)
		labels.append(label)


	data = np.array(data, dtype="float32")
	labels = np.array(labels)
	return data, labels

	