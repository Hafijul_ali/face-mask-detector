from serializer import serialize_data
from preprocessor import augment_label_data
from model import get_model
from trainer import train_model
from model import save_model
from statistics import model_report
from statistics import plot_graph

BATCH_SIZE=32
INITIAL_LEARNING_RATE = 1e-4
EPOCHS = 20


path=""
model_name="mask_detector_model"
plot_name="model_train_test_plot"

if __name__ == "__main__":
	print("[INFO] loading images...")
	data, labels = serialize_data(path)

	print("[INFO] labelling data...")
	trainX, testX, trainY, testY,	aug, label_binarizer	= augment_label_data(data, labels)

	print("[INFO] building model...")
	model = get_model(INITIAL_LEARNING_RATE, EPOCHS)

	print("[INFO] training head...")
	train_history = train_model(model, trainX, testX, trainY, testY, aug)

	print("[INFO] saving model...")
	save_model(model, model_name)

	print("[INFO] evaluating model...")
	model_report(model, testX, label_binarizer, BATCH_SIZE)

	print("[INFO] plotting graphs...")
	plot_graph(train_history, plot_name, EPOCHS)
