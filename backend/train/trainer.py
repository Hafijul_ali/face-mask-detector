from tensorflow.keras.preprocessing.image import (
	ImageDataGenerator, 
	img_to_array, 
	load_img
)
from tensorflow.keras.applications import MobileNetV2


BATCH_SIZE = 32
EPOCHS = 20



def train_model(model, trainX, testX, trainY, testY, aug):
	train_history = model.fit(
		aug.flow(trainX, trainY, batch_size=BATCH_SIZE),
		steps_per_epoch=len(trainX) // BATCH_SIZE,
		validation_data=(testX, testY),
		validation_steps=len(testX) // BATCH_SIZE,
		epochs=EPOCHS)
	return train_history



