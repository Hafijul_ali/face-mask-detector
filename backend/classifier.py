from tensorflow.keras.preprocessing import image
import numpy as np
#from backend.deeplearning import graph,output_list
from tensorflow.keras.models import load_model


model = load_model('backend/models/mask_detector.h5')

def classify(image_data='', model_path=''):
    global model

    if not model_path.endswith('model1.hdf5'):
        model = load_model(model_path)

    img = image.img_to_array(image_data)
    img = np.expand_dims(img, axis=0)
    img = img/255
    
    prediction = model.predict(img)

    prediction_flatten = prediction.flatten()
    max_val_index = np.argmax(prediction_flatten)
    status = "Mask" if max_val_index == 0 else "No Mask"
    confidence = prediction_flatten[max_val_index]*100
    result = "Status: {0}\nConfidence: {1:.2f}%".format(status,confidence)
    
    return result
